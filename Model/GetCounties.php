<?php

namespace Plugin\jtl_land_switcher\Model;

use JTL\Shop;
use PDO;

class GetCounties
{
    /**
     * @return array
     */
    public static function execute(): array
    {
        $db = Shop::Container()->getDB();
        $pdo = $db->getPDO();
        $sql = "SELECT cISO, cEnglisch FROM tland WHERE cISO NOT IN (SELECT tland_ciso FROM jtl_land_switcher_link)";
        $stmt = $pdo->query($sql);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}