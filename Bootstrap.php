<?php

namespace Plugin\jtl_land_switcher;

use JTL\Plugin\Bootstrapper;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Laminas\Diactoros\ServerRequestFactory;
use Plugin\jtl_land_switcher\Controller\ModelBackendController;
use function Functional\first;

class Bootstrap extends Bootstrapper
{
    /**
     * @inheritdoc
     */
    public function renderAdminMenuTab(string $tabName, int $menuID, JTLSmarty $smarty): string
    {
        $plugin = $this->getPlugin();
        if ($tabName === 'Land switcher') {
            $controller = new ModelBackendController(
                $this->getDB(),
                $this->getCache(),
                Shop::Container()->getAlertService(),
                Shop::Container()->getAdminAccount(),
                Shop::Container()->getGetText()
            );
            $controller->menuID = $menuID;
            $controller->plugin = $plugin;
            $request = ServerRequestFactory::fromGlobals($_SERVER, $_GET, $_POST, $_COOKIE, $_FILES);
            $response = $controller->getResponse($request, [], $smarty);
            if (count($response->getHeader('location')) > 0) {
                \header('location:' . first($response->getHeader('location')));
                exit();
            }
            return (string)$response->getBody();
        }
        return '';
    }
}