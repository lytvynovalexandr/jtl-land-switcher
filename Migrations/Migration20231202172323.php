<?php declare(strict_types=1);

namespace Plugin\jtl_land_switcher\Migrations;

use JTL\Plugin\Migration;
use JTL\Update\IMigration;

/**
 * Class Migration20231202172323
 * @package Plugin\jtl_land_switcher\Migrations
 */
class Migration20231202172323 extends Migration implements IMigration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute(
            'CREATE TABLE IF NOT EXISTS `jtl_land_switcher_link` (
                  `id` int(10) NOT NULL AUTO_INCREMENT,
                  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `tland_ciso` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `link` (`link`),
                  UNIQUE KEY `tland_ciso` (`tland_ciso`),
                  KEY `land_switcher_link_ciso_tland_ciso` (`tland_ciso`),
                  CONSTRAINT `land_switcher_link_ciso_tland_ciso` FOREIGN KEY (`tland_ciso`) REFERENCES `tland` (`cISO`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->doDeleteData()) {
            $this->execute('DROP TABLE IF EXISTS `jtl_land_switcher_link`');
        }
    }
}
